<?php
	class DbConnection {

		static $instance = null;
	
		public static function connect(){
			return self::getInstance();
		}
	
		public static function getInstance(){
			
			if(!self::$instance instanceof PDO){
				$db = new PDO('mysql:host=localhost;dbname=db_pests;charset=utf8', 'petsadmin', '1c^Ey,#%rWp[1c^Ey,#%rWp[');
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
				
				if($db) {
					self::$instance = $db;
				}
			}

			return self::$instance;
		}		

 }

?>


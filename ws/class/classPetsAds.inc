<?php

	include_once("classEntity.inc");

	/**
	* @author Chandima.Ranaweera
	*/
	class PetsAds
	{
		public $id;
		public $ad_wants;
		public $ad_type;
		public $catergory;
		public $ad_photo1;
		public $ad_photo2;
		public $ad_photo3;
		public $ad_photo4;
		public $breed;
		public $size;
		public $age;
		public $title;
		public $descreption;
		public $price;
		public $city;
		public $address;
		public $name;
		public $phone;
		public $email;
		public $published_date;
		public $exp_date;
		public $status;

		public function __construct()
		{	
			$this->published_date=date("d-m-Y h:m");
			$this->exp_date = date('Y-m-d H:i:s', strtotime($this->published_date . ' + 30 day'));		
		}

		public function setId($value)
		{
			$this->id = $value;
			return $this;
		}

		public function setAdWants($value)
		{
			$this->ad_wants = $value;
			return $this;
		}

		public function setAdType($value)
		{
			$this->ad_type = $value;
			return $this;
		}

		public function setCatergory($value)
		{
			$this->catergory = $value;
			return $this;
		}

		public function setAdPhoto1($value)
		{
			$this->ad_photo1 = $value;
			return $this;
		}

		public function setAdPhoto2($value)
		{
			$this->ad_photo2 = $value;
			return $this;
		}

		public function setAdPhoto3($value)
		{
			$this->ad_photo3 = $value;
			return $this;
		}

		public function setAdPhoto4($value)
		{
			$this->ad_photo4 = $value;
			return $this;
		}

		public function setBreed($value)
		{
			$this->breed = $value;
			return $this;
		}

		public function setSize($value)
		{
			$this->size = $value;
			return $this;
		}

		public function setAge($value)
		{
			$this->age = $value;
			return $this;
		}

		public function setTitle($value)
		{
			$this->title = $value;
			return $this;
		}

		public function setDescription($value)
		{
			$this->descreption = $value;
			return $this;
		}

		public function setPrice($value)
		{
			$this->price = $value;
			return $this;
		}

		public function setCity($value)
		{
			$this->city = $value;
			return $this;
		}

		public function setAddress($value)
		{
			$this->address = $value;
			return $this;
		}

		public function setName($value)
		{
			$this->name = $value;
			return $this;
		}

		public function setPhone($value)
		{
			$this->phone = $value;
			return $this;
		}

		public function setEmail($value)
		{
			$this->email = $value;
			return $this;
		}

		public function setPublishedDate($value)
		{
			$this->published_date = $value;
			return $this;
		}

		public function setExpireDate($value)
		{
			$this->exp_date = $value;
			return $this;
		}

		public function setStatus($value)
		{
			$this->status = $value;
			return $this;
		}

		public function insert()
		{			

			$affected_rows = 0;

			try {
								
				$db = DbConnection::getInstance();

				$stmt = $db->prepare("INSERT INTO tbl_petsads (ad_wants, ad_type, catergory, ad_photo1, ad_photo2, ad_photo3, ad_photo4, breed, size, age, title, descreption, price, city, address, name, phone, email, published_date, exp_date) VALUES (:ad_want ,:ad_type, :catergory, :im1, :im2, :im3, :im4, :subcat, :dog_size, :dog_age, :title, :descreption, :price, :district, :address, :name, :phone, :email, :pdate, :edate);");
					$stmt->execute(array(':ad_want' 	=> $this->ad_wants, 
										 ':ad_type' 	=> $this->ad_type,
										 ':catergory' 	=> $this->catergory,
										 ':im1' 	=> $this->ad_photo1,
										 ':im2' 	=> $this->ad_photo2,
										 ':im3'	=> $this->ad_photo3,
										 ':im4' 	=> $this->ad_photo4,
										 ':subcat' 		=> $this->breed,
										 ':dog_size'		=> $this->size,
										 ':dog_age' 		=> $this->age,
										 ':title' 		=> $this->title,
										 ':descreption' => $this->descreption,
										 ':price' 		=> $this->price,
										 ':district' 		=> $this->city,
										 ':address' 	=> $this->address,
										 ':name' 		=> $this->name,
										 ':phone' 		=> $this->phone,
										 ':email' 		=> $this->email,
										 ':pdate' => $this->published_date,
										 ':edate' 	=> $this->exp_date));
					$affected_rows = $stmt->rowCount();	

			} catch (PDOException $e) {
				throw  new Exception(__METHOD__.' could not insert petsads '.$e->getMessage(),_status_not_created);
						}
			return $affected_rows > 0;
		}

		public static function get($id)
		{
			$db = DbConnection::getInstance();			
			$stmt = $db->prepare("SELECT * FROM tbl_petsads WHERE id=:id");
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			$stmt->setFetchMode(PDO::FETCH_INTO, new PetsAds());
			$stmt->execute();
			$ad = $stmt->fetch();

			return $ad;
		}

		public static function getAll()
		{
			$ads = array();
			try {
				$db = DbConnection::getInstance();
				$stmt = $db->prepare("SELECT * FROM tbl_petsads");				
				$stmt->execute();
				$ads = $stmt->fetchAll(PDO::FETCH_CLASS, "PetsAds");
				
			} catch (PDOException $e) {
				throw  new Exception(__METHOD__.' could not get catergory '.$e->getMessage(),_status_not_found);
			}

			return $ads;
		}
	}
?>
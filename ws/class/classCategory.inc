<?php
	class Category{

		public $id;
		public $catergory;
		public $status;

		public function __construct(){

		}

		public function setId($id)
		{
			$this->id = $id;
			return $this;
		}

		public function setCategory($cat)
		{
			$this->catergory = $cat;
			return $this;
		}

		public function setStatus($status)
		{
			$this->status = $status;
			return $this;
		}

		public static function get($id)
		{
			$db = DbConnection::getInstance();			
			$stmt = $db->prepare("SELECT * FROM tbl_catergory WHERE id=:id");
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			$stmt->setFetchMode(PDO::FETCH_INTO, new Category());
			$stmt->execute();
			$cat = $stmt->fetch();

			return $cat;
		}

		public static function getAll()
		{
			$cats = array();
			try {
				$db = DbConnection::getInstance();
				$stmt = $db->prepare("SELECT * FROM tbl_catergory");		
				//$stmt->setFetchMode(PDO::FETCH_INTO, new Category());
				$stmt->execute();
				$cats = $stmt->fetchAll(PDO::FETCH_CLASS, "Category");
				
			} catch (PDOException $e) {
				throw  new Exception(__METHOD__.' could not get catergory '.$e->getMessage(),_status_not_found);
			}

			return $cats;
		}

		

		public function insert()
		{
			$affected_rows = 0;

			try {
								
				$db = DbConnection::getInstance();

				$stmt = $db->prepare("INSERT INTO tbl_catergory(catergory , status) VALUES(:catergory,:status)");
					$stmt->execute(array(':catergory' => $this->catergory, 
											':status' => $this->status));
					$affected_rows = $stmt->rowCount();	

			} catch (PDOException $e) {
				throw  new Exception(__METHOD__.' could not insert catergory '.$e->getMessage(),_status_not_created);
						}
			return $affected_rows > 0;
		}

		
		public function update()
		{
			$affected_rows = 0;
			try {
							
				$cat = Category::get($this->id);

				if($this->status == NULL) {
					$this->status = $cat->status;
				}

				if($this->catergory == NULL) {
					$this->catergory = $cat->catergory;
				}

				$db = DbConnection::getInstance();
				$stmt = $db->prepare("UPDATE tbl_catergory SET catergory=?, status=?  WHERE id=?");
				$stmt->execute(array($this->catergory, $this->status, $this->id));
				$affected_rows = $stmt->rowCount();
			
			} catch (PDOException  $e) {
				throw  new Exception(__METHOD__.'could not update catergory',_status_not_created);
			}

			return $affected_rows > 0;
		}


	}
?>
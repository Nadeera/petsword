<?php
	/**
	* @author Chandima.Ranaweera
	* @version 1.0
	*/
	class SubCategory
	{
		public $id;
		public $main_cat;
		public $sub_cat;
		public $sub_cat_name;
		public $status;
		
		function __construct()
		{
			
		}

		public function setId($value)
		{
			$this->id = $value;
			return $this;
		}

		public function setMainCategory($value)
		{
			$this->main_cat = $value;
			return $this;
		}

		public function setSubCategory($value)
		{
			$this->sub_cat = $value;
			return $this;
		}

		public function setSubCategoryName($value)
		{
			$this->sub_cat_name = $value;
			return $this;
		}

		public function setStaus($value)
		{
			$this->status = $value;
			return $this;
		}

		public static function getAll()
		{
			$cats = array();
			try {
				$db = DbConnection::getInstance();
				$stmt = $db->prepare("SELECT * FROM tbl_subcatergory");		
				//$stmt->setFetchMode(PDO::FETCH_INTO, new Category());
				$stmt->execute();
				$cats = $stmt->fetchAll(PDO::FETCH_CLASS, "SubCategory");
				
			} catch (PDOException $e) {
				throw  new Exception(__METHOD__.' could not get sub catergory '.$e->getMessage(),_status_not_found);
			}

			return $cats;
		}

		public static function getByCategory($cat)
		{
			$cats = array();
			try {
				$db = DbConnection::getInstance();
				$stmt = $db->prepare("SELECT * FROM tbl_subcatergory WHERE main_cat=:mainCat");		
				//$stmt->setFetchMode(PDO::FETCH_INTO, new Category());
				$stmt->execute(array(':mainCat'=>$cat));
				$cats = $stmt->fetchAll(PDO::FETCH_CLASS, "SubCategory");
				
			} catch (PDOException $e) {
				throw  new Exception(__METHOD__.' could not get sub catergory '.$e->getMessage(),_status_not_found);
			}

			return $cats;
		}
	}
?>
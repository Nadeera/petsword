<?php
/**
 * @author Chandima.Ranaweera
 */	
	
	include_once 'class/classPetsAds.inc';
	$data = array();	
	
	 try {
                if(substr(__ROUTER_PATH,0,16) == '/ws/petsads/find'){
                	
                        $matches = array();
                                		
                                $data = PetsAds::getAll(); 
                                
                                if(empty($data))
                                {
                                	throw new Exception(__METHOD__.'category not found',_status_not_found);
                                }

                                throw new Exception(__METHOD__.'category found',_status_success);
                                              
                }

                else if(substr(__ROUTER_PATH,0,15) == '/ws/petsads/get')
                {                
                        $matches = array();
                        if(preg_match('/^\/ws\/category\/get\/([0-9a-z-_A-Z]{1,32})$/', __ROUTER_PATH, $matches)) {                        		
                                $data = PetsAds::get($matches[1]); 
                                //echo json_encode($data);
                                if(empty($data))
                                {
                                	throw new Exception(__METHOD__.'category not found',_status_not_found);
                                }

                                throw new Exception(__METHOD__.'category found',_status_success);
                        }
                        else {
                                throw new Exception(__METHOD__.'category not sfecific found',_status_not_found);
                        }
                }

                else if(substr(__ROUTER_PATH,0,15) == '/ws/petsads/add')
                {

                	$ad = new PetsAds();
                       
                        
                        $id = $ad_wants = $ad_type = $catergory = $ad_photo1 = $ad_photo2 = null;
                        $ad_photo3 = $ad_photo4 = $breed = $size = $age = $title = $descreption = null;
                        $price = $city = $address = $name = $phone = $email = null;
                        $status = $filenames = null;
                        
                        extract($_POST,EXTR_IF_EXISTS);


                        $ad_photo1 = $filenames[0];  
                        $ad_photo2 = $filenames[0];
                        $ad_photo3 = $filenames[0];
                        $ad_photo4 = $filenames[0];
                     
                        if (sizeof($filenames) > 1) {
                              $ad_photo2 = $filenames[1];
                        }
                        if (sizeof($filenames) > 2) {
                              $ad_photo3 = $filenames[2];
                        }
                        if (sizeof($filenames) > 3) {
                              $ad_photo4 = $filenames[3];
                        }
                       
                       
                        $ad->setAdWants($ad_wants)->setAdType($ad_type)->setCatergory($catergory);
                        $ad->setAdPhoto1($ad_photo1)->setAdPhoto2($ad_photo2)->setAdPhoto3($ad_photo3);
                        $ad->setAdPhoto4($ad_photo4)->setBreed($breed)->setSize($size)->setAge($age);
                        $ad->setTitle($title)->setDescription($descreption)->setPrice($price)->setCity($city);
                        $ad->setAddress($address)->setName($name)->setPhone($phone)->setEmail($email);
                        

                       

                        
                       
                        //echo "in adds";

                        
                        $ad->insert();
                        throw new Exception(__METHOD__.'ad inserted', _status_success);

                }
                else if (__ROUTER_PATH == '/ws/petsads/upload') 
                {
                       
                            $error = false;
                            $files = array();

                            $uploaddir = '../images/petsads/';
                            foreach($_FILES as $file)
                            {
                                if(move_uploaded_file($file['tmp_name'], $uploaddir .basename($file['name'])))
                                {
                                    $files[] = $uploaddir .$file['name'];
                                    $data = $files;
                                    throw new Exception(__METHOD__.'file uploaded', _status_success);
                                }
                                else
                                {
                                    throw new Exception(__METHOD__.'petsad upload failed', _status_not_found);
                                }
                            }                      
                                       
                }
        }
        catch (Exception $e)
        {
        	header('Content-Type:application/json');
                echo json_encode(array(
                                'msg' => $e->getMessage(),
                				'code' => $e->getCode(),
                				'data'=>$data
                ));
        }	

?>	

<?php
/**
 * @author Chandima.Ranaweera
 */	
	
	include_once 'class/classCategory.inc';
	$data = array();	
	
	 try {
                if(substr(__ROUTER_PATH,0,17) == '/ws/category/find'){
                	
                        $matches = array();
                                		
                                $data = Category::getAll(); 
                                
                                if(empty($data))
                                {
                                	throw new Exception(__METHOD__.'category not found',_status_not_found);
                                }

                                throw new Exception(__METHOD__.'category found',_status_success);
                                              
                }

                else if(substr(__ROUTER_PATH,0,16) == '/ws/category/get'){
                		//echo json_encode($data);
                        $matches = array();
                        if(preg_match('/^\/ws\/category\/get\/([0-9a-z-_A-Z]{1,32})$/', __ROUTER_PATH, $matches)) {                        		
                                $data = Category::get($matches[1]); 
                                //echo json_encode($data);
                                if(empty($data))
                                {
                                	throw new Exception(__METHOD__.'category not found',_status_not_found);
                                }

                                throw new Exception(__METHOD__.'category found',_status_success);
                        }
                        else {
                                throw new Exception(__METHOD__.'category not sfecific found',_status_not_found);
                        }
                }

                else if(substr(__ROUTER_PATH,0,16) == '/ws/category/add'){
                	    $cat = new Category();
                        $id = $category =$status= null;
                        extract($_POST,EXTR_IF_EXISTS);
                        $cat->setCategory($category)->setStatus($status)->insert();
                        throw new Exception(__METHOD__.'category inserted', _status_success);

                }
        }
        catch (Exception $e){
        		header('Content-Type:application/json');
                echo json_encode(array(
                                'msg' => $e->getMessage(),
                				'code' => $e->getCode(),
                				'data'=>$data
                ));
        }	

?>	

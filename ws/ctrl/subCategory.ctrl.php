<?php
/**
 * @author Chandima.Ranaweera
 */	
	
	include_once 'class/classSubCategory.inc';
	$data = array();	
	
	 try {
	 		if(__ROUTER_PATH == '/ws/subcategory/find')
	 		{
                	 $data = SubCategory::getAll(); 
                                if(empty($data))
                                {
                                	throw new Exception(__METHOD__.'category not found',_status_not_found);
                                }

                                throw new Exception(__METHOD__.'category found',_status_success);
                                              
                
            }

            else if(substr(__ROUTER_PATH,0,20) == '/ws/subcategory/find')
            {
                	
                if(preg_match('/^\/ws\/subcategory\/find\/([0-9a-z-_A-Z]{1,32})$/', __ROUTER_PATH, $matches)) 
    			{                        		
                    $data = SubCategory::getByCategory($matches[1]);                               
                    
                    if(empty($data))
                    {
                    	throw new Exception(__METHOD__.'category not found',_status_not_found);
                    }

                    throw new Exception(__METHOD__.'category found',_status_success);
                 }
                  else 
                  {
                    throw new Exception(__METHOD__.'category not sfecific found',_status_not_found);
                  }    
            }

            else if(substr(__ROUTER_PATH,0,19) == '/ws/subcategory/get')
            	{
                		//echo json_encode($data);
                        $matches = array();
                        if(preg_match('/^\/ws\/subcategory\/get\/([0-9a-z-_A-Z]{1,32})$/', __ROUTER_PATH, $matches)) {                        		
                                $data = SubCategory::get($matches[1]); 
                                //echo json_encode($data);
                                if(empty($data))
                                {
                                	throw new Exception(__METHOD__.'category not found',_status_not_found);
                                }

                                throw new Exception(__METHOD__.'category found',_status_success);
                        }
                        else {
                                throw new Exception(__METHOD__.'category specified not found',_status_not_found);
                        }
                }

                else if(substr(__ROUTER_PATH,0,19) == '/ws/subcategory/add'){
                	    $cat = new SubCategory();
                       	$id = $main_cat = $sub_cat = $sub_cat_name = $status = null;
                        extract($_POST,EXTR_IF_EXISTS);
                        $cat->setMainCategory($main_cat)->setSubCategory($sub_cat)->
                        setSubCategoryName($sub_cat_name)->setStatus($status)->insert();
                        throw new Exception(__METHOD__.'sub category inserted', _status_success);

                }
        }
        catch (Exception $e){
        		header('Content-Type:application/json');
                echo json_encode(array(
                                'msg' => $e->getMessage(),
                				'code' => $e->getCode(),
                				'data'=>$data
                ));
        }	

?>	

<?php
/**
 * @author Chandima Ranaweera
 * @version 1.0
 */
	include_once "common/classDbConnection.inc";

	ini_set('display_errors', 1);
	error_reporting(E_ALL);
	date_default_timezone_set("Asia/Colombo");
	/*
	 * status codes 
	 */
	define('_status_success', 1000);
	define('_status_mismatch', 1001);
	define('_status_not_created', 1002);
	define('_status_not_updated', 1003);
	define('_status_not_found', 1004);
	define('_status_already_exists', 1005);
	
	/*
	 * end status codes
	 */
	
	$GLOBALS['data'] =array();    //json Array
	//
	define('__ROUTER_PATH', '/'.trim((string)parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH),'/'));
	define('__METHOD', $_SERVER['REQUEST_METHOD']);	
	
	//
	switch (__ROUTER_PATH) {
		case '/':
			echo "we are on root";
			break;
		case '/ws':
			echo "we are on ws";
			break;
		case '/ws/category/add':
		case '/ws/category/find':
		case '/ws/category/get':	
		case (substr(__ROUTER_PATH,0,16) == '/ws/category/get'):		
		case (substr(__ROUTER_PATH,0,17) == '/ws/category/find'):		
			include_once "ctrl/category.ctrl.php";
			break;	
		case '/ws/petsads/add':
		case '/ws/petsads/find':
		case '/ws/petsads/get':
		case '/ws/petsads/upload':
		case (substr(__ROUTER_PATH,0,15) == '/ws/petsads/add'):		
		case (substr(__ROUTER_PATH,0,15) == '/ws/petsads/get'):		
		case (substr(__ROUTER_PATH,0,16) == '/ws/petsads/find'):	
			include_once "ctrl/petsAds.ctrl.php";
			break;
		case '/ws/subcategory/find':
		case '/ws/subcategory/get':
		case '/ws/subcategory/add':		
		case (substr(__ROUTER_PATH,0,20) == '/ws/subcategory/find'):	
		case (substr(__ROUTER_PATH,0,19) == '/ws/subcategory/get'):	
		case (substr(__ROUTER_PATH,0,19) == '/ws/subcategory/add'):	
			include_once "ctrl/subCategory.ctrl.php";
			break;
		default:
			echo __ROUTER_PATH;
			break;
	}
?>

<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="css/vendor/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/toastr.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/form.css">
    <link rel="stylesheet" href="css/dropzone/dropzone.min.css">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

  </head>
  <body>
    <section>
      <header>
        <div class="head-left"></div>
        <div class="head-right"></div>
      </header>
    </section>
    <!--end of header-->

    <section class="tp-nav">
      <nav class="navbar navbar-default tp-navbar-custm">
        <div class="tp-nav-left col-lg-9">
          <div class="container-fluid"> 
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> 
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
              </button>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav tp-nav-nav">
                  <li class="active"><a href="index.php">HOME<span class="sr-only">(current)</span></a></li>
                  <li><a href="#">ABOUT US</a></li>
                  <li class="dropdown"> 
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PETS <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="sellers.php">SELLER</a></li>
                      <li><a href="buyers.php">BUYERS</a></li>                
                    </ul>
                  </li>
                  <li class="dropdown"> 
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PRODUCTS <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="pet-shops.php">PET SHOPS</a></li>
                      <li><a href="pets-products.php">PET PRODUCTS</a></li>
                    </ul>
                  </li>
                  <li><a href="ask-a-vets.php">ASK A VET</a></li>
                  <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">STORIES <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="events.php">RECENT EVENTS</a></li>
                      <li><a href="stories.php">PET STORIES</a></li>                
                    </ul>
                  </li>
                  <li><a href="contact-us.php">CONTACT US</a></li>
              </ul>
            </div><!-- /.navbar-collapse --> 
          </div><!-- /.container-fluid --> 
        </div>
        <div class="tp-nav-right col-lg-3">
          <a href="all-ad.php">
            <div class="tp-all-btn">All Ads</div>
          </a>
          <a href="post-ad.php">
            <div class="tp-post-btn">Post Your Ad</div>
          </a>
        </div>
      </nav>
    </section>

    <section class="tp-content">
      <div class="lizard"></div>
    
      <div class="section-container">
        <div class="ads_tilte title-div">Post Your Advertisment Here</div>
        <br/>
        <?php include("post-ad-form.php"); ?>
      </div>

      <div class="ui-panel is-transparent t-small conditions">
        <div class="ui-panel-content ui-panel-block">
          <p><em>By posting this ad, you agree to the Terms &amp; Conditions of this site.</em></p>
        </div>
      </div>

      <div class="section-container ">     
        <div class="ads_tilte">Ad Posting Rules</div>
        <div class="row section-rules">
          <p style="padding:20px; padding-bottom:10px !important; font-size:22px;"> By posting this ad, you aggree to the rules &amp; conditions of this site, </p>
          <ul style="margin-bottom:30px;">
            <li>Make sure you post in the correct category.</li>
            <li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
            <li>Do not upload pictures with watermarks.</li>
            <li>Do not post ads containing multiple items unless it's a package deal.</li>
            <li>Do not put your email or phone numbers in the title or description.</li>
          </ul>          
        </div>
      </div>
    
    </section>
    <footer>
      <div  class="tp-ftr-top">
        
      </div>
      <div class="tp-ftr-btm">
    	  <div class="tp-ftr-lnks">
          <ul>
            <li><a href="#">HOME</a></li>
            <li><a href="#">ABOUT US</a></li>
            <li><a href="#">PETS</a></li>
            <li><a href="#">PRODUCTS</a></li>
            <li><a href="#">ASK A VET</a></li>
            <li><a href="#">STORIES</a></li>
            <li><a href="#">CONTACT US</a></li>
          </ul>
        </div>
        <div class="tp-ftr-ryt col-lg-12"> © 2015 www.petsoworld.com All Rights Reserved. </div>
        </div>
    </footer>

    <script src="js/vendor/jquery-1.11.2.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/vendor/dropzone/dropzone.min.js"></script>
    <script src="js/vendor/toastr.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>

<?php include("../db_connect.php"); ?>
<?php if(!isset($_SESSION)){ session_start(); } 
if(!isset($_SESSION['ses_username'])){ ?>
<script type="text/javascript">window.location = "index.php"</script><?php } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Petsoworlds</title>
<link type="text/css" href="style.css" rel="stylesheet" />
<script language="JavaScript" type="text/javascript">
function checkDelete(){
    return confirm('Are you sure?');
}
</script>
<script>
function showUser(str) {
  if (str=="") {
    document.getElementById("txtHint").innerHTML="";
    return;
  } 
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","change_status.php?q="+str,true);
  xmlhttp.send();
}

function sliderlinkupdate(str) {
	str=encodeURIComponent(str).replace(/\-/g, "%2D").replace(/\_/g, "%5F").replace(/\./g, "%2E").replace(/\!/g, "%21").replace(/\~/g, "%7E").replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\(/g, "%28").replace(/\)/g, "%29");
  if (str=="") {
    document.getElementById("txtHint").innerHTML="";
    return;
  } 
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","change_slider_link.php?q="+str,true);
  xmlhttp.send();
}

function slidertextupdate(str) {
	str=encodeURIComponent(str).replace(/\-/g, "%2D").replace(/\_/g, "%5F").replace(/\./g, "%2E").replace(/\!/g, "%21").replace(/\~/g, "%7E").replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\(/g, "%28").replace(/\)/g, "%29");
  if (str=="") {
    document.getElementById("txtHint").innerHTML="";
    return;
  } 
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","change_slider_text.php?q="+str,true);
  xmlhttp.send();
}

</script>

</head>

<body>
<div id="header">Admin Panel</div>
    <?php include("left_navigation.php"); 
	 ?>
    
    <div id="right_navigation">
    	<div id="control_bar">
        	<a href="new_slider.php"><div id="button">Add New Slider Image</div></a>
        	<a href="slider_management.php"><div id="button">View Slider</div></a>
        </div>
        
        <div style="width:1110px; margin-left:10px; color:#090; border-bottom:#090 1px solid; float:left; height:25px;">
        <span id="txtHint"></span>
        </div>
        
        <?php
			$sql_slider=mysql_query("SELECT * FROM  tbl_slider");
			while($rw_slides=mysql_fetch_array($sql_slider)){
				$slider_img=$rw_slides['slider_img'];
				$status=$rw_slides['status'];
				$id=$rw_slides['id'];
		?>
				<div id="slider_box">
                	<div style="width:230px; height:107px; margin:10px; float:left;">
                    	<img width="100%" height="100%" src="../images/slider/<?php echo $slider_img; ?>" />
                    </div>
                    
                    <a href="del_slide.php?id=<?php echo $id; ?>"  onClick="return checkDelete()">
                    <div id="del_button">Delete Slide</div>
                    </a>
                    
                    <?php if($status==1){ ?>                    
                    <div style="width:25px; margin-left:10px; float:left; height:25px;">
                    	<img width="100%" height="100%" src="images/ecMjkp7cn.png" />
                    </div>
                    <?php } ?>
                    
                    <div style="width:80px; float:right; height:25px;">
                        <select name="ch_status" id="ch_status" onChange="showUser(this.value)" style="font-size:12px; height:20px;">
                        	<option value="Select">Select</option>
                        	<option value="1<?php echo ",".$id; ?>">Active</option>
                            <option value="0<?php echo ",".$id; ?>">Inactive</option>
                        </select>
                    </div>
                    <input type="text" name="" id="" style="width:98%; margin-top:5px;" value="<?php echo $rw_slides['slider_text']; ?>" placeholder="Caption" onKeyUp="slidertextupdate(this.value+'<?php echo ",".$id; ?>')"/>
                    <input type="text" name="" id="" style="width:98%;  margin-top:5px;" value="<?php echo $rw_slides['slider_link']; ?>" placeholder="Link URL" onKeyUp="sliderlinkupdate(this.value+'<?php echo ",".$id; ?>')"/>
                </div>
        <?php	
			}
		?>
    	
    </div>
    
    
</body>
</html>
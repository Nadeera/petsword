<?php include("../db_connect.php"); ?>
<?php if(!isset($_SESSION)){ session_start(); } 
if(!isset($_SESSION['ses_username'])){ ?>
<script type="text/javascript">window.location = "index.php"</script><?php } ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Petsoworlds</title>
<link type="text/css" href="style.css" rel="stylesheet" />
<script language="JavaScript" type="text/javascript">
function checkDelete(){
    return confirm('Are you sure?');
}


function showUser(str) {
  if (str=="") {
    document.getElementById("txtHint").innerHTML="";
    return;
  } 
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","change_status_products_ads.php?q="+str,true);
  xmlhttp.send();
}

function sliderlinkupdate(str) {
	str=encodeURIComponent(str).replace(/\-/g, "%2D").replace(/\_/g, "%5F").replace(/\./g, "%2E").replace(/\!/g, "%21").replace(/\~/g, "%7E").replace(/\*/g, "%2A").replace(/\'/g, "%27").replace(/\(/g, "%28").replace(/\)/g, "%29");
  if (str=="") {
    document.getElementById("txtHint").innerHTML="";
    return;
  } 
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
    }
  }
  xmlhttp.open("GET","change_status_products_ads.php?q="+str,true);
  xmlhttp.send();
}


</script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>

</head>

<body>
<div id="header">Admin Panel</div>
    <?php include("left_navigation.php"); ?>
    
    <?php   

	?>   
    
    
    
    
<div id="right_navigation">
    	<div id="control_bar">
        	<a href="new-pet-products.php"><div id="button">New Pet Products</div></a>
        	<a href="products-ads-view-all.php"><div id="button">View All</div></a>
        </div>
        
        <div style="width:980px; margin-left:10px; color:#090; float:left; height:25px;">
        <span id="txtHint"></span>
        </div>
        
	  <div id="slider_box" style="width:100%; border:none; font-size:18px;">
		<table width="1019" border="0">
  <tr>
    <td height="30" colspan="2" style="border-bottom:#09C 1px dashed ; font-weight:bold;">All Petshop Ads</td>
    <td colspan="2"style="border-bottom:#09C 1px dashed;">&nbsp; <span id="txtHint"></span></td>
    <td width="97"style="border-bottom:#09C 1px dashed;">&nbsp;</td>
    <td width="75"style="border-bottom:#09C 1px dashed;">&nbsp;</td>
    <td width="134"style="border-bottom:#09C 1px dashed;">&nbsp;</td>
    <td width="89"style="border-bottom:#09C 1px dashed;">&nbsp;</td>
    <td width="57"style="border-bottom:#09C 1px dashed;">&nbsp;</td>
    <td width="57"style="border-bottom:#09C 1px dashed;">&nbsp;</td>
  </tr>
  <tr>
    <td width="100" style="border-bottom:#09C 1px dashed;">Date</td>
    <td width="84" height="35" style="border-bottom:#09C 1px dashed;">Ad Type</td>
    <td width="181" style="border-bottom:#09C 1px dashed;">Title</td>
    <td width="103" style="border-bottom:#09C 1px dashed;">City &amp; Addres</td>
    <td style="border-bottom:#09C 1px dashed;">Name</td>
    <td style="border-bottom:#09C 1px dashed;">Phone </td>
    <td style="border-bottom:#09C 1px dashed;">Email</td>
    <td style="border-bottom:#09C 1px dashed;">Thumbnil</td>
    <td style="border-bottom:#09C 1px dashed;">Status</td>
    <td style="border-bottom:#09C 1px dashed;">Delete</td>
  </tr>
    <?php
	$qry=mysql_query("SELECT * FROM tbl_pestproducts_ads order by published_date DESC");
	while($rw=mysql_fetch_array($qry)){
		$id=$rw['id'];
	?>
  <tr style="font-size:12px;">
    <td><?php echo $rw['published_date']; ?></td>
    <td><?php if($rw['ad_type']==0){ echo "Free Ad";}else{ echo "Top Ad";} ?></td>
    <td><?php echo substr($rw['title'],0,200); ?></td>
    <td><?php echo $rw['city'].", ". $rw['address']; ?></td>
    <td><?php echo $rw['name']; ?></td>
    <td><?php echo $rw['phone']; ?></td>
    <td><?php echo $rw['email']; ?></td>
    <td><div style="width:70px; height:60px;"><img width="100%" height="100%" src="../images/pet-shops-ads/<?php echo $rw['ad_photo1']; ?>" /></div></td>
    <td><select name="ch_status" id="ch_status" onChange="showUser(this.value)" style="font-size:12px; height:20px;">
    						<?php if($rw['status']==1){ ?>
                        	<option value="1<?php echo ",".$id; ?>">Active</option>
                            <option value="0<?php echo ",".$id; ?>">Inactive</option>
                            <?php }else{ ?>
                            <option value="0<?php echo ",".$id; ?>">Inactive</option>
                        	<option value="1<?php echo ",".$id; ?>">Active</option>
                            <?php } ?>
                        </select></td>
    <td style="text-align:center;"><a href="del_pet_products_ads.php?id=<?php echo $rw['id']; ?>"  onClick="return checkDelete()">Delete</a></td>
  </tr>
    <?php	
	}
	?>

</table>

      </div>
    	
    </div>
    
    
</body>
</html>
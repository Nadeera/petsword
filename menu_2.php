<nav class="navbar navbar-default" id="primary-navbar">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav nav-pills nav-menu">
        <li class="active list-menu"><a href="index.php" style="color:black">HOME</a></li>
        <li class="list-menu"><a href="#" style="outline: none">ASK A VET</a></li>
        <li class="list-menu"><a href="#">PET SELLERS</a></li>
        <li class="dropdown list-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PRODUCTS <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">PET SHOPS</a></li>
            <li><a href="#">PET PRODUCTS</a></li>
            <!--<li role="separator" class="divider"></li>-->
          </ul>
        </li>
        <li class="dropdown list-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">STORIES <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">RECENT EVENTS</a></li>
            <li><a href="#">PET STORIES</a></li>
          </ul>
        </li>
        <li class="list-menu"><a href="#">ABOUT US</a></li>
        <li class="list-menu"><a href="#">CONTACT US</a></li>
        <li role="presentation" class="active"><a href="#" class="all_ad_btt">All Ads</a></li>
        <li role="presentation" class="active"><a href="#" class="post_ad_btt">Post Your Ad</a></li>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet">
<link rel='stylesheet' media='screen and (min-width: 1024px)' href='css/style.css' />
<link href='http://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="js/slippry.min.js"></script>
<script src="js/slippry.min.js"></script>
<link rel="stylesheet" href="css/slippry.css">
</head>

<body>

    <div id="header">
    	<div id="site_title"><img src="images/logo-x2.png" /></div>
        <div id="parrot"></div>
    </div>
    
    <?php include("menu.php"); ?>

    <div class="container">
    	<div id="ads_tilte">Ask A Vet</div>
		<div class="row">
			<div id="vet_questions">Q : Is your pet dog healthy and happy?</div>
            <div id="vet_answer">Do you ever wonder about your dog, asking yourself, ‘Is it alright?’ Why is he laying like that? Has he got a problem? No longer will you be worrying over small problems your dog has, because you can follow these simple routine and daily check-ups to keep a happy and healthy canine pal.
        </div>
        
			<div id="vet_questions">Q : Are you looking after your pet bird properly?</div>
            <div id="vet_answer">If you have a pet bird, you know that you want to keep him/her safe from harm, right? This article will show you how to keep your feathery little friend flying safe.
        </div>
		</div>
    </div>
    
    
    <?php include("footer.php"); ?>
<script src="js/bootstrap.min.js"></script>
    
    
</body>
</html>
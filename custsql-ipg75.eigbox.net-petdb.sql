-- phpMyAdmin SQL Dump
-- version 2.8.0.1
-- http://www.phpmyadmin.net
-- 
-- Host: custsql-ipg75.eigbox.net
-- Generation Time: Jun 21, 2015 at 11:14 AM
-- Server version: 5.5.32
-- PHP Version: 4.4.9
-- 
-- Database: `db_pests`
-- 
CREATE DATABASE `db_pests` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_pests`;

-- --------------------------------------------------------

-- 
-- Table structure for table `ask_stories`
-- 

CREATE TABLE `ask_stories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(500) NOT NULL,
  `intro_text` longtext NOT NULL,
  `images` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `ask_stories`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `ask_vets`
-- 

CREATE TABLE `ask_vets` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(500) NOT NULL,
  `intro_text` longtext NOT NULL,
  `images` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `ask_vets`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_catergory`
-- 

CREATE TABLE `tbl_catergory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `catergory` varchar(500) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `tbl_catergory`
-- 

INSERT INTO `tbl_catergory` VALUES (1, 'Dogs', 1);
INSERT INTO `tbl_catergory` VALUES (2, 'Cats', 1);
INSERT INTO `tbl_catergory` VALUES (3, 'Birds', 1);
INSERT INTO `tbl_catergory` VALUES (4, 'Fish', 1);
INSERT INTO `tbl_catergory` VALUES (5, 'Others', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_city`
-- 

CREATE TABLE `tbl_city` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `City` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

-- 
-- Dumping data for table `tbl_city`
-- 

INSERT INTO `tbl_city` VALUES (67, 'Anuradapura');
INSERT INTO `tbl_city` VALUES (68, 'Badulla');
INSERT INTO `tbl_city` VALUES (69, 'Batticaloa');
INSERT INTO `tbl_city` VALUES (70, 'Colombo');
INSERT INTO `tbl_city` VALUES (71, 'Galle');
INSERT INTO `tbl_city` VALUES (72, 'Gampaha');
INSERT INTO `tbl_city` VALUES (73, 'Hambantota');
INSERT INTO `tbl_city` VALUES (74, 'Jaffna');
INSERT INTO `tbl_city` VALUES (75, 'Kalutara');
INSERT INTO `tbl_city` VALUES (76, 'Kandy');
INSERT INTO `tbl_city` VALUES (77, 'Kegalle');
INSERT INTO `tbl_city` VALUES (78, 'Kilinochchi');
INSERT INTO `tbl_city` VALUES (79, 'Kurunegala');
INSERT INTO `tbl_city` VALUES (80, 'Mannar');
INSERT INTO `tbl_city` VALUES (81, 'Matale');
INSERT INTO `tbl_city` VALUES (82, 'Matara');
INSERT INTO `tbl_city` VALUES (83, 'Moneragala');
INSERT INTO `tbl_city` VALUES (84, 'Mullaitivu');
INSERT INTO `tbl_city` VALUES (85, 'Nuwara Eliya');
INSERT INTO `tbl_city` VALUES (86, 'Polonnaruwa');
INSERT INTO `tbl_city` VALUES (87, 'Puttalam');
INSERT INTO `tbl_city` VALUES (88, 'Ratnapura');
INSERT INTO `tbl_city` VALUES (89, 'Trincomalee');
INSERT INTO `tbl_city` VALUES (90, 'Vavuniya');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_operators`
-- 

CREATE TABLE `tbl_operators` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `passwd` varchar(500) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `tbl_operators`
-- 

INSERT INTO `tbl_operators` VALUES (1, 'admin', '0192023a7bbd73250516f069df18b500', 1);
INSERT INTO `tbl_operators` VALUES (2, 'damith', '764d04d29bcb3b8c6c6d4009ee77309b', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_pestproducts_ads`
-- 

CREATE TABLE `tbl_pestproducts_ads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_type` int(1) NOT NULL,
  `ad_photo1` varchar(1000) NOT NULL,
  `ad_photo2` varchar(1000) NOT NULL,
  `ad_photo3` varchar(1000) NOT NULL,
  `ad_photo4` varchar(1000) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `descreption` longtext NOT NULL,
  `city` varchar(1000) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `published_date` varchar(20) NOT NULL,
  `exp_date` varchar(20) NOT NULL,
  `status` int(1) NOT NULL,
  `price` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `tbl_pestproducts_ads`
-- 

INSERT INTO `tbl_pestproducts_ads` VALUES (5, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 0, '');
INSERT INTO `tbl_pestproducts_ads` VALUES (6, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 1, '');
INSERT INTO `tbl_pestproducts_ads` VALUES (7, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 1, '');
INSERT INTO `tbl_pestproducts_ads` VALUES (8, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 1, '');
INSERT INTO `tbl_pestproducts_ads` VALUES (9, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 1, '');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_pestshops_ads`
-- 

CREATE TABLE `tbl_pestshops_ads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_type` int(1) NOT NULL,
  `ad_photo1` varchar(1000) NOT NULL,
  `ad_photo2` varchar(1000) NOT NULL,
  `ad_photo3` varchar(1000) NOT NULL,
  `ad_photo4` varchar(1000) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `descreption` longtext NOT NULL,
  `city` varchar(1000) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `published_date` varchar(20) NOT NULL,
  `exp_date` varchar(20) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

-- 
-- Dumping data for table `tbl_pestshops_ads`
-- 

INSERT INTO `tbl_pestshops_ads` VALUES (4, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 0);
INSERT INTO `tbl_pestshops_ads` VALUES (5, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 0);
INSERT INTO `tbl_pestshops_ads` VALUES (6, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 1);
INSERT INTO `tbl_pestshops_ads` VALUES (7, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 0);
INSERT INTO `tbl_pestshops_ads` VALUES (8, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 1);
INSERT INTO `tbl_pestshops_ads` VALUES (9, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 1);
INSERT INTO `tbl_pestshops_ads` VALUES (10, 0, 'Chrysanthemum.jpg', 'Desert.jpg', 'Hydrangeas.jpg', 'Jellyfish.jpg', 'Test Title', 'Test Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test TitleTest Title Test Title Test Title Test Title', 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '17-06-2015 11:06', '2015-07-17 11:06:00', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_petsads`
-- 

CREATE TABLE `tbl_petsads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_wants` int(1) NOT NULL,
  `ad_type` int(1) NOT NULL,
  `catergory` varchar(500) NOT NULL,
  `ad_photo1` varchar(500) NOT NULL,
  `ad_photo2` varchar(500) NOT NULL,
  `ad_photo3` varchar(500) NOT NULL,
  `ad_photo4` varchar(500) NOT NULL,
  `breed` varchar(500) NOT NULL,
  `size` varchar(500) NOT NULL,
  `age` varchar(500) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `descreption` varchar(2000) NOT NULL,
  `price` decimal(65,0) NOT NULL,
  `city` varchar(500) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `name` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `published_date` varchar(100) NOT NULL,
  `exp_date` varchar(100) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- 
-- Dumping data for table `tbl_petsads`
-- 

INSERT INTO `tbl_petsads` VALUES (16, 1, 1, 'Dogs', 'GermanShep1_wb.jpg', 'GermanShep1_wb.jpg', 'GermanShep1_wb.jpg', 'GermanShep1_wb.jpg', 'crossbreed', '10 inches', '03 Months', 'Cross Breed Dogs For Sale', 'Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale Cross Breed Dogs For Sale ', 0, 'Colombo', 'Gampaha', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '19-06-2015 08:06', '2015-07-19 08:06:00', 1);
INSERT INTO `tbl_petsads` VALUES (17, 0, 1, 'Dogs', '406006_1280x720.jpg', '406006_1280x720.jpg', '406006_1280x720.jpg', '406006_1280x720.jpg', 'goldenretriever', '10', '03 Months', 'Golden Retriever Dogs For Sale', 'Golden Retriever Dogs For Sale Golden Retriever Dogs For Sale Golden Retriever Dogs For Sale Golden Retriever Dogs For Sale Golden Retriever Dogs For Sale Golden Retriever Dogs For Sale Golden Retriever Dogs For Sale Golden Retriever Dogs For Sale ', 0, 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '19-06-2015 08:06', '2015-07-19 08:06:00', 1);
INSERT INTO `tbl_petsads` VALUES (18, 0, 1, 'Birds', 'parrot.jpg', 'parrot.jpg', 'parrot.jpg', 'parrot.jpg', '', '10', '03 Months', 'Parrots For Sale', 'Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale Parrots For Sale ', 0, 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '19-06-2015 08:06', '2015-07-19 08:06:00', 1);
INSERT INTO `tbl_petsads` VALUES (19, 1, 0, 'Dogs', 'Guide-Dogs_025-11.jpg', 'Guide-Dogs_025-11.jpg', 'Guide-Dogs_025-11.jpg', 'Guide-Dogs_025-11.jpg', 'goldenretriever', '10 inches', '03 Months', 'Golden Retriever Dogs For Sale', 'LABRADOR MALE DOG FORE STUD K.A.S.L REGISTERED . 1 ST PRIZE WINNER (79TH / 80TH / 81TH SHOWS) . IMPORTED BLOOD LINE', 0, 'Colombo', 'Colombo 02', 'Damith Marathunga', '0777798354', 'damssuse@gmail.com', '20-06-2015 11:06', '2015-07-20 11:06:00', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_petshops`
-- 

CREATE TABLE `tbl_petshops` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_loc` int(3) NOT NULL,
  `ad_url` varchar(3000) NOT NULL,
  `ad_link` varchar(3000) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=482 DEFAULT CHARSET=latin1 AUTO_INCREMENT=482 ;

-- 
-- Dumping data for table `tbl_petshops`
-- 

INSERT INTO `tbl_petshops` VALUES (479, 1, 'thumb-391.jpg', 'dddddd', 1);
INSERT INTO `tbl_petshops` VALUES (480, 1, '86509417.jpg', 'ffff', 1);
INSERT INTO `tbl_petshops` VALUES (481, 1, '10521841_690070854379414_5369568358861574287_n.png', 'Hacked', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_slider`
-- 

CREATE TABLE `tbl_slider` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `slider_img` varchar(500) NOT NULL,
  `status` int(1) NOT NULL,
  `slider_link` varchar(3000) NOT NULL,
  `slider_text` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=376 DEFAULT CHARSET=latin1 AUTO_INCREMENT=376 ;

-- 
-- Dumping data for table `tbl_slider`
-- 

INSERT INTO `tbl_slider` VALUES (372, 'slide-21.jpg', 1, 'test', 'Transitioning your pet to a new world');
INSERT INTO `tbl_slider` VALUES (373, 'slide-13.jpg', 1, 'www.google.lk', 'Transitioning your pet to a new world');
INSERT INTO `tbl_slider` VALUES (374, 'slide-3.jpg', 1, '', '');
INSERT INTO `tbl_slider` VALUES (375, 'slide-4.jpg', 1, '', 'Kitty is my best friend');

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_subcatergory`
-- 

CREATE TABLE `tbl_subcatergory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `main_cat` varchar(500) NOT NULL,
  `sub_cat` varchar(500) NOT NULL,
  `sub_cat_name` varchar(1000) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

-- 
-- Dumping data for table `tbl_subcatergory`
-- 

INSERT INTO `tbl_subcatergory` VALUES (1, 'Dogs', 'crossbreed', 'Cross Breed', 1);
INSERT INTO `tbl_subcatergory` VALUES (2, 'Dogs', 'goldenretriever', 'Golden Retriever', 1);
INSERT INTO `tbl_subcatergory` VALUES (3, 'Dogs', 'rottweiler', 'Rottweiler', 1);
INSERT INTO `tbl_subcatergory` VALUES (5, 'Dogs', 'lion shepahd', 'Lion Shepahd', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `tbl_vet_ads`
-- 

CREATE TABLE `tbl_vet_ads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_loc` int(3) NOT NULL,
  `ad_url` varchar(3000) NOT NULL,
  `ad_link` varchar(3000) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=481 DEFAULT CHARSET=latin1 AUTO_INCREMENT=481 ;

-- 
-- Dumping data for table `tbl_vet_ads`
-- 

INSERT INTO `tbl_vet_ads` VALUES (478, 1, 'thumb-391.jpg', 'google', 1);
INSERT INTO `tbl_vet_ads` VALUES (479, 1, 'thumb-341.jpg', 'dddddddd', 1);
INSERT INTO `tbl_vet_ads` VALUES (480, 1, 'thumb-371.jpg', 'dfgdfgdfg', 1);

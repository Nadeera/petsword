-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 11, 2015 at 06:04 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_petsoworlds`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_catergory`
--

CREATE TABLE IF NOT EXISTS `tbl_catergory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `catergory` varchar(500) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_catergory`
--

INSERT INTO `tbl_catergory` (`id`, `catergory`, `status`) VALUES
(1, 'Dogs', 1),
(2, 'Cats', 1),
(3, 'Birds', 1),
(4, 'Fish', 1),
(5, 'Others', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE IF NOT EXISTS `tbl_city` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `City` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `tbl_city`
--

INSERT INTO `tbl_city` (`id`, `City`) VALUES
(67, 'Anuradapura'),
(68, 'Badulla'),
(69, 'Batticaloa'),
(70, 'Colombo'),
(71, 'Galle'),
(72, 'Gampaha'),
(73, 'Hambantota'),
(74, 'Jaffna'),
(75, 'Kalutara'),
(76, 'Kandy'),
(77, 'Kegalle'),
(78, 'Kilinochchi'),
(79, 'Kurunegala'),
(80, 'Mannar'),
(81, 'Matale'),
(82, 'Matara'),
(83, 'Moneragala'),
(84, 'Mullaitivu'),
(85, 'Nuwara Eliya'),
(86, 'Polonnaruwa'),
(87, 'Puttalam'),
(88, 'Ratnapura'),
(89, 'Trincomalee'),
(90, 'Vavuniya');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_operators`
--

CREATE TABLE IF NOT EXISTS `tbl_operators` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `passwd` varchar(500) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_operators`
--

INSERT INTO `tbl_operators` (`id`, `username`, `passwd`, `status`) VALUES
(1, 'admin', '245922219138ab28b8d874d4d1ed0c9c', 1),
(2, 'damith', '764d04d29bcb3b8c6c6d4009ee77309b', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_petsads`
--

CREATE TABLE IF NOT EXISTS `tbl_petsads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_wants` int(1) NOT NULL,
  `ad_type` int(1) NOT NULL,
  `catergory` varchar(500) NOT NULL,
  `ad_photo1` varchar(500) NOT NULL,
  `ad_photo2` varchar(500) NOT NULL,
  `ad_photo3` varchar(500) NOT NULL,
  `ad_photo4` varchar(500) NOT NULL,
  `breed` varchar(500) NOT NULL,
  `size` varchar(500) NOT NULL,
  `age` varchar(500) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `descreption` varchar(2000) NOT NULL,
  `price` decimal(65,0) NOT NULL,
  `city` varchar(500) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `name` varchar(500) NOT NULL,
  `phone` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `published_date` varchar(100) NOT NULL,
  `exp_date` varchar(100) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_petsads`
--

INSERT INTO `tbl_petsads` (`id`, `ad_wants`, `ad_type`, `catergory`, `ad_photo1`, `ad_photo2`, `ad_photo3`, `ad_photo4`, `breed`, `size`, `age`, `title`, `descreption`, `price`, `city`, `address`, `name`, `phone`, `email`, `published_date`, `exp_date`, `status`) VALUES
(1, 1, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '2015-06-02 10.42 AM', '', 1),
(2, 1, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '', '', 1),
(3, 1, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '', '', 1),
(4, 1, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '', '', 1),
(5, 0, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study - Wanted', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '', '', 1),
(6, 1, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '', '', 1),
(7, 1, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '', '', 1),
(8, 0, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study - Wanted', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '', '', 1),
(9, 1, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '', '', 1),
(10, 0, 1, 'Dogs', 'SG Lion Vom Bellington.jpg', 'GermanShep1_wb.jpg', 'pj-the-german-shepherd-7_69796_2013-03-17_w450.jpg', 'German-Shepherd.jpg', 'Cross', '2.5', '03 Months', 'Lion German Shepherd male dog for study - Wanted', 'not for sale  healthy male lion german shephard dog for stud (cross) .2 and half years old.', '15000', 'Kalutara', 'Kalutara', 'Mr. Sandaruwan', '077 7777 555', 'sandaruwan@gmail.com', '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_petshops`
--

CREATE TABLE IF NOT EXISTS `tbl_petshops` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_loc` int(3) NOT NULL,
  `ad_url` varchar(3000) NOT NULL,
  `ad_link` varchar(3000) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=481 ;

--
-- Dumping data for table `tbl_petshops`
--

INSERT INTO `tbl_petshops` (`id`, `ad_loc`, `ad_url`, `ad_link`, `status`) VALUES
(479, 1, 'thumb-391.jpg', 'dddddd', 1),
(480, 1, '86509417.jpg', 'ffff', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_slider`
--

CREATE TABLE IF NOT EXISTS `tbl_slider` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `slider_img` varchar(500) NOT NULL,
  `status` int(1) NOT NULL,
  `slider_link` varchar(3000) NOT NULL,
  `slider_text` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=374 ;

--
-- Dumping data for table `tbl_slider`
--

INSERT INTO `tbl_slider` (`id`, `slider_img`, `status`, `slider_link`, `slider_text`) VALUES
(372, 'slide-21.jpg', 1, 'test', 'Transitioning your pet to a new world'),
(373, 'slide-13.jpg', 1, '', 'Transitioning your pet to a new world');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subcatergory`
--

CREATE TABLE IF NOT EXISTS `tbl_subcatergory` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `main_cat` varchar(500) NOT NULL,
  `sub_cat` varchar(500) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `tbl_subcatergory`
--

INSERT INTO `tbl_subcatergory` (`id`, `main_cat`, `sub_cat`, `status`) VALUES
(1, 'Dogs', 'Cross Breed', 1),
(2, 'Dogs', 'aaaa', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vet_ads`
--

CREATE TABLE IF NOT EXISTS `tbl_vet_ads` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_loc` int(3) NOT NULL,
  `ad_url` varchar(3000) NOT NULL,
  `ad_link` varchar(3000) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=481 ;

--
-- Dumping data for table `tbl_vet_ads`
--

INSERT INTO `tbl_vet_ads` (`id`, `ad_loc`, `ad_url`, `ad_link`, `status`) VALUES
(478, 1, 'thumb-391.jpg', 'google', 1),
(479, 1, 'thumb-341.jpg', 'dddddddd', 1),
(480, 1, 'thumb-371.jpg', 'dfgdfgdfg', 1);

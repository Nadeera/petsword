<nav class="navbar navbar-default" role="navigation" style="margin-bottom:20px;">

   <div class="navbar-header">

      <button type="button" class="navbar-toggle" data-toggle="collapse" 

         data-target="#example-navbar-collapse">

         <span class="sr-only">Toggle navigation</span>

         <span class="icon-bar"></span>

         <span class="icon-bar"></span>

         <span class="icon-bar"></span>

      </button>

      <a class="navbar-brand" href="#"></a>

   </div>

   <div class="collapse navbar-collapse" id="example-navbar-collapse">

      <ul class="nav navbar-nav">

         <li class="active"><a href="index.php">HOME</a></li>

         <li><a href="about-us.php">ABOUT US</a></li>

         <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

               PETS <b class="caret"></b>

            </a>

            <ul class="dropdown-menu">

               <li><a href="sellers.php">SELLER</a></li>

               <li><a href="buyers.php">BUYERS</a></li>

            </ul>

         </li>

         <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

               PRODUCTS <b class="caret"></b>

            </a>

            <ul class="dropdown-menu">

               <li><a href="pet-shops.php">PET SHOPS</a></li>

               <li><a href="pets-products.php">PET PRODUCTS</a></li>

            </ul>

         </li>

         <li><a href="ask-a-vets.php">ASK A VET</a></li>

         <li class="dropdown">

            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

               STORIES <b class="caret"></b>

            </a>

            <ul class="dropdown-menu">

               <li><a href="events.php">RECENT EVENTS</a></li>

               <li><a href="stories.php">PET STORIES</a></li>

            </ul>

         </li>

         <li><a href="contact-us.php">CONTACT US</a></li>

      </ul>

   </div>
	<a href="post-ad.php">
<div class="post_ad_btt">Post Your Ad</div>
</a>
	<a href="all-ad.php">
<div class="all_ad_btt">All Ads</div>
</a>
</nav>  



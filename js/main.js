// Variable to store your files
var files = [];

$(document).ready(function () {
	Dropzone.autoDiscover = false;
	loadCategory();

	// Add events
	$('input[type=file]').on('change', prepareUpload);

	$('form').on('submit', uploadFiles);

	$('#catergory').on('change', function () {
		var text =$(this).find("option:selected").text();
		if (text == 'Dogs') {

			loadSubCategory();

			if ($('.dog-food').hasClass('dog-food')) {
				$('.dog-food').removeClass('hide');
			};
		}else	
		{
			$('.dog-food').addClass('hide');
		}
	})
	//var myDropzone = new Dropzone("#my-drop-zone" , { url: "/ws/petsads/add"});
});

// Grab the files and set them to our variable
function prepareUpload(event)
{
  files.push(event.target.files[0]);
}


function loadSubCategory () {
	
	$.ajax({
	  url: "/ws/subcategory/find",
	  context: document.body
	}).done(function(data) {
		var options = $("#breed");
		options.html('');
	  $.each(data.data, function(index, item) {
		    options.append($("<option />").val(item.sub_cat).text(item.sub_cat));
		});
	});
}

function loadCategory () {
	
	$.ajax({
	  url: "/ws/category/find",
	  context: document.body
	}).done(function(data) {
		var options = $("#catergory");

	  $.each(data.data, function(index, item) {
		    options.append($("<option />").val(item.catergory).text(item.catergory));
		});
	});
}

// Catch the form submit and upload the files
function uploadFiles(event)
{
  event.stopPropagation(); // Stop stuff happening
    event.preventDefault(); // Totally stop stuff happening

    // START A LOADING SPINNER HERE

    // Create a formdata object and add the files
    var data = new FormData();
    $.each(files, function(key, value)
    {
        data.append(key, value);
    });

    $.ajax({
        url: '/ws/petsads/upload',
        type: 'POST',
        data: data,
        cache: false,
        dataType: 'json',
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(data, textStatus, jqXHR)
        {
            if(typeof data.error === 'undefined')
            {
                // Success so call function to process the form
                submitForm(event, data);
            }
            else
            {
                // Handle errors here
                console.log('ERRORS: ' + data.error);
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            console.log(jqXHR.responseText);
            // STOP LOADING SPINNER
        }
    });
}

function Submit () {
	
}

function submitForm(event, data)
{
	
  // Create a jQuery object from the form
    $form = $(event.target);

    // Serialize the form data
    var formData = $form.serialize();

    // You should sterilise the file names
    $.each(data.data, function(key, value)
    {
        formData = formData + '&filenames[]=' + value;
    });

    $.ajax({
        url: '/ws/petsads/add',
        type: 'POST',
        data: formData,
        cache: false,
        dataType: 'json',
        success: function(data, textStatus, jqXHR)
        {
            if(typeof data.error === 'undefined')
            {
                toastr.success('Your ad has sent to review successfully!', 'Petsword');
                //alert('Your ad has sent to review successfully!');
                console.log('SUCCESS: ' + data.success);
                resetForm();
            }
            else
            {
                // Handle errors here
                console.log('ERRORS: ' + data.error);
                //alert('Your ad has could not be added');
                toastr.error('Your ad has could not be added', 'Petsword');
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            toastr.error('Your ad has could not be added', 'Petsword');
            //alert('Your ad has could not be added');
        },
        complete: function()
        {
            // STOP LOADING SPINNER
        }
    });
}

function resetForm () {
	$( 'form' ).each(function(){
    	this.reset();
	});
}
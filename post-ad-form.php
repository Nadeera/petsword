        <!--<div class="form-class">-->
          <form class="post-form" action="/ws/petsads/add">
            <input type="hidden" name="ad_wants" id="ad_wants" value="0">
            <div class="form-group">
              <h3 class="content-heading">Advertisment Type</h3>
              <div class="radio-div">
              <label class="radio-inline">
                <input type="radio" name="ad_type" id="ad_type1" value="0" >Top Ads
              </label>
              <label class="radio-inline free-ad-radio">
                <input type="radio" name="ad_type" id="ad_type2" value="1" checked>Free Ads
              </label>
              </div>
            </div>

            <div class="form-group">
               <h3 class="content-heading">Fill in Details</h3>
            </div>
            <div class="row">
              <div class="form-group col-md-8">  
               
                <select id="catergory" name="catergory" class="form-control" required>
                  <option>Type of Animal*</option>                 
                </select>
                </div>
                <div class="col-md-4"></div>
              </div>

              <div class="row">
                <div class="form-group col-md-8 ">
                  <div class="fallback">
                    <input name="ad_photo1" type="file" />
                  </div>
                </div>
                <div class="col-md-4"></div>
              </div>

              <div class="row">
                <div class="form-group col-md-8">
                  <div class="fallback">
                    <input name="ad_photo2" type="file"/>
                  </div>
                </div>
                <div class="col-md-4"></div>
              </div>

              <div class="row">
                <div class="form-group col-md-8">
                  <div class="fallback">
                    <input name="ad_photo3" type="file" />
                  </div>
                </div>
                <div class="col-md-4"></div>
              </div>

              <div class="row">
                <div class="form-group col-md-8">
                  <div class="fallback">
                    <input name="ad_photo4" type="file" />
                  </div>
                </div>
                <div class="col-md-4"></div>
              </div>
              
              <div class="dog-food hide">
                <div class="row">
                  <div class="form-group col-md-8">
                    <select id="breed" name="breed" class="form-control">
                    </select>
                  </div>
                  <div class="col-md-4"></div>
                </div>

                <div class="row">
                  <div class="form-group col-md-8">
                    <input type="text" class="form-control" name="size" id="size" placeholder="Dog Size*" >
                  </div> 
                  <div class="col-md-4">                  
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-8">  
                    <input type="text" class="form-control" name = "age" id="age" placeholder="Dog Age*" >
                  </div> 
                  <div class="col-md-4">                  
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-8">
                  <input type="text" class="form-control" name="title" id="title" placeholder="Title*" required>
                </div>
                <div class="col-md-4 help-block">
                  <div class="info-message">Keep it short!</div>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-8">  
                  <textarea class="form-control" rows="5" name="descreption" id="descreption" placeholder="Description*" required></textarea>
                </div> 
                <div class="col-md-4 help-block"> 
                  More details = more interested buyers!                 
                </div>
              </div>
              <div class="row">
                <div class="col-md-8">
                  <div class="form-inline">
                    <input class="form-control price" id="fields-price-amount" name="price" type="number" placeholder="Price (Rs) *" required>
                    <input type="checkbox" class="price-checkbox"> Negotiable
                  </div>
                </div>
                <div class="col-md-4 help-block">
                  Pick a good price.
                </div>
              </div>

              <div class="form-group">
                <h3 class="content-heading">Where is your location?</h3>
              </div>

              <div class="row">
              <div class="form-group col-md-8">  
                <select id="city" name="city" class="form-control" required>
                  <option>Select city or division</option>                 
                  <option>Colombo</option>                 
                </select>
                </div>
                <div class="col-md-4"></div>
              </div>

              <div class="row">
                <div class="form-group col-md-8">
                  <input type="text" class="form-control" name="address" id="address" placeholder="Address (Optional)">
                </div>
                <div class="col-md-4"></div>
              </div>

              <div class="form-group">
                <h3 class="content-heading">About you</h3>
              </div>

              <div class="row">
                <div class="form-group col-md-8">
                  <input type="text" class="form-control" name="name" id="name" placeholder="Name*" required>
                </div>
                <div class="col-md-4"></div>
              </div>

              <div class="row">
                <div class="form-group col-md-8">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email*" required>
                </div>
                <div class="col-md-4"></div>
              </div>

              <div class="row">
                <div class="form-group col-md-8">
                  <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone No*" required>
                </div> 
                <div class="col-md-4 help-block">  
                  Make sure to check your phone number! You must enter a phone number so that ikman.lk can contact you, but you can choose not to display it on your ad.                
                </div>
              </div>
              
              <div class="row">
                <div class="form-group checkbox col-md-4">
                  <label>
                    <input type="checkbox"> I do not want to be contacted by telemarketers.
                  </label>
                </div>

                <div class="form-group post-btt col-md-4">
                  <button type="submit" class="btn btn-default">Post Ads</button>
                </div>
              </div>

          </form>
        <!--</div>-->

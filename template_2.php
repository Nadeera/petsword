<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">    
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Post Your Add | Petsworld</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/custom.css" rel="stylesheet" />
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>


  </head>
  <body class = "template-body">
  	<div id ="page">
  		<header  class="site-header">
  			<div class="header">
  				<div class="container">
  					<div class="row">
  						<div class="col-md-12"> 
  							<div class="site-logo">
  								<img src="images/logo-x2.png" alt="Paws & Claws">
  							</div>
  							<?php include("menu_2.php"); ?>

  						</div><!--end of col-md-12 div-->
  						
  					</div><!--end of row div-->

  				</div><!--end of container div-->


  			</div><!--end of class = header div-->
  		
			<div class="container"><!--parrot container-->
				<div class="row">
					<div class="col-mid-12">
						<div class="animal-parrot-left"></div>
					</div><!--end of parrot col-md-12 div-->
				</div><!--end of parrot row div-->
			</div><!--end of parrot container div-->
		</header>

			<div id="lizard"></div>
			<div id="ads_tilte">Post Your Advertisment Here</div>
			<div class="row"></div>
		</div>
	<?php include("footer_2.php"); ?>
	</div> <!--page div-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet">
<link rel='stylesheet' media='screen and (min-width: 1024px)' href='css/style.css' />
<link href='http://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script src="js/slippry.min.js"></script>
<script src="js/slippry.min.js"></script>
<link rel="stylesheet" href="css/slippry.css">
</head>

<body>

    <div id="header">
    	<div id="site_title"><img src="images/logo-x2.png" /></div>
        <div id="parrot"></div>
    </div>
    
    <?php include("menu.php"); ?>

    <div class="container">
    	<div id="ads_tilte">Behaviour Training</div>
		<div class="row">
			<div id="stories_image"><img width="100%" height="100%" src="images/thumb-341.jpg" /></div>
            <div id="story_descption">Caring for your dog’s teeth and gums can avoid serious health problems. You should brush your dog’s teeth at least 2 or 3 times a week. There are toothbrushes and toothpaste made specifically for dogs. Do not use human toothpaste. There is a product you can add to your dog’s water bowl that will help reduce tartar. And of course your vet will professionally clean your dog’s teeth. Check with your vet before using any product for your dog.
            
            <input type="submit" class="button_text"  value="View"  />         
        </div>
		</div>
    </div>
    
    <div class="container" style="margin-top:20px;">
    	<div id="ads_tilte">Nutrition</div>
		<div class="row">
			<div id="stories_image"><img width="100%" height="100%" src="images/thumb-341.jpg" /></div>
            <div id="story_descption">Caring for your dog’s teeth and gums can avoid serious health problems. You should brush your dog’s teeth at least 2 or 3 times a week. There are toothbrushes and toothpaste made specifically for dogs. Do not use human toothpaste. There is a product you can add to your dog’s water bowl that will help reduce tartar. And of course your vet will professionally clean your dog’s teeth. Check with your vet before using any product for your dog.
            
            <input type="submit" class="button_text"  value="View"  />         
        </div>
		</div>
    </div>
    
    <?php include("footer.php"); ?>
<script src="js/bootstrap.min.js"></script>
    
    
</body>
</html>
<?php include("db_connect.php"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/bootstrap.css" rel="stylesheet">
<link rel='stylesheet' media='screen and (min-width: 1024px)' href='css/style.css' />
<link href='http://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="js/slippry.min.js"></script>
<script src="js/slippry.min.js"></script>
<link rel="stylesheet" href="css/slippry.css">

    <!-- Insert to your webpage before the </head> -->
    <script src="carouselengine/amazingcarousel.js"></script>
    <link rel="stylesheet" type="text/css" href="carouselengine/initcarousel-1.css">
    <script src="carouselengine/initcarousel-1.js"></script>
    <!-- End of head section HTML codes -->

<script>
jQuery(document).ready(function ($) {

  //$('#checkbox').change(function(){
    setInterval(function () {
        moveRight();
    }, 3000);
  //});
  
	var slideCount = $('#slider ul li').length;
	var slideWidth = $('#slider ul li').width();
	var slideHeight = $('#slider ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#slider').css({ width: slideWidth, height: slideHeight });
	
	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#slider ul li:last-child').prependTo('#slider ul');

    function moveLeft() {
		$( "#slider ul li" ).fadeOut( "slow", function() {
    		$('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
  		});
	};

    function moveRight() {
        $('#slider ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider ul li:first-child').appendTo('#slider ul');
            $('#slider ul').css('left', '');
        });
    };

    $('a.control_prev').click(function () {
        moveLeft();
    });

    $('a.control_next').click(function () {
        moveRight();
    });

}); 




$("#slideshow > div:gt(0)").hide();

setInterval(function() { 
  $('#slideshow > div:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#slideshow');
},  3000);

$("#slideshow1 > div:gt(0)").hide();

setInterval(function() { 
  $('#slideshow1 > div:first')
    .fadeOut(1000)
    .next()
    .fadeIn(1000)
    .end()
    .appendTo('#slideshow1');
},  3000);

   
</script>

<script>
jQuery(document).ready(function ($) {

  //$('#checkbox').change(function(){
    setInterval(function () {
        moveRight();
    }, 3000);
  //});
  
	var slideCount = $('#slider_vet ul li').length;
	var slideWidth = $('#slider_vet ul li').width();
	var slideHeight = $('#slider_vet ul li').height();
	var sliderUlWidth = slideCount * slideWidth;
	
	$('#slider_vet').css({ width: slideWidth, height: slideHeight });
	
	$('#slider_vet ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
	
    $('#slider_vet ul li:last-child').prependTo('#slider_vet ul');

    function moveLeft() {
        $('#slider_vet ul').animate({
            left: + slideWidth
        }, 200, function () {
            $('#slider_vet ul li:last-child').prependTo('#slider_vet ul');
            $('#slider_vet ul').css('left', '');
        });
    };

    function moveRight() {
        $('#slider_vet ul').animate({
            left: - slideWidth
        }, 200, function () {
            $('#slider_vet ul li:first-child').appendTo('#slider_vet ul');
            $('#slider_vet ul').css('left', '');
        });
    };

    $('a.control_prev').click(function () {
        moveLeft();
    });

    $('a.control_next').click(function () {
        moveRight();
    });

});    
</script>




</head>
<body>
	
    <div id="header">
    	<div id="site_title"><img src="images/logo-x2.png" /></div>
        <div id="parrot"></div>
    </div>
    
    <?php include("menu.php"); ?>
    
    <div id="slider_main"><?php include("slider.php"); ?></div>
    
    <?php include("home_top_ads.php"); ?>
    
    
    <div class="container" style="margin-top:20px;">
    	<div id="dog"></div>
		<div class="row">
        	<div class="col-md-6" style="margin-bottom:5px; ">
    			<div id="ads_tilte" style="background:#39C url(images/foot-inverse.png);">Pet Shops And Pet Products</div>
                <div id="services_slider">
                    <div id="slideshow">
                   	<?php
					$sql_slider=mysql_query("SELECT * FROM tbl_petshops WHERE status=1");
					while($rw_slider=mysql_fetch_array($sql_slider)){
                    $slider_link=$rw_slider['ad_link'];
                    if($slider_link!=""){
                ?>                        

                           <div><a href="<?php echo $slider_link; ?>"><img style="border:0px solid #39C;"  width="100%" height="100%" src="images/tbl_vet_ads/<?php echo $rw_slider['ad_url']; ?>"  /></a>    </div>                     
          <?php }else{ ?>
                           <div><img style="border:0px solid #39C;"  width="100%" height="100%" src="images/tbl_vet_ads/<?php echo $rw_slider['ad_url']; ?>"  /></div>                     
           <?php 	  
			  }
			  } ?> 
                    </div>
                 </div>
			</div>
        	<div class="col-md-6" style="margin-bottom:5px;">
            	
    			<div id="ads_tilte" style="background:url(images/foot-inverse.png) #69C;">Ask a Vet And Events</div>
					<div class="row" style="margin-top:0px;">
                <div id="services_slider">

                        <div id="slideshow1">
					<?php
					$sql_slider=mysql_query("SELECT * FROM tbl_vet_ads WHERE status=1");
					while($rw_slider=mysql_fetch_array($sql_slider)){
                    $slider_link=$rw_slider['ad_link'];
                    if($slider_link!=""){
                ?>                        
                        
                           <div><a href="<?php echo $slider_link; ?>"><img style="border:0px solid #39C;"  width="100%" height="100%" src="images/tbl_vet_ads/<?php echo $rw_slider['ad_url']; ?>"  /></a>    </div>                     
          <?php }else{ ?>
                           <div><img style="border:0px solid #39C;"  width="100%" height="100%" src="images/tbl_vet_ads/<?php echo $rw_slider['ad_url']; ?>"  /></div>                     
           <?php 	  
			  }
			  } ?> 
                        </div>

                 </div>
                    </div>
                    </div>

			</div>
        </div>
    </div>    
    
    <?php include("footer.php"); ?>
    
  </body>
<script src="js/bootstrap.min.js"></script>

		<script>
			$(function() {
				var demo1 = $("#demo1").slippry({
					// transition: 'fade',
					// useCSS: true,
					// speed: 1000,
					// pause: 3000,
					// auto: true,
					// preload: 'visible',
					// autoHover: false
				});

				$('.stop').click(function () {
					demo1.stopAuto();
				});

				$('.start').click(function () {
					demo1.startAuto();
				});

				$('.prev').click(function () {
					demo1.goToPrevSlide();
					return false;
				});
				$('.next').click(function () {
					demo1.goToNextSlide();
					return false;
				});
				$('.reset').click(function () {
					demo1.destroySlider();
					return false;
				});
				$('.reload').click(function () {
					demo1.reloadSlider();
					return false;
				});
				$('.init').click(function () {
					demo1 = $("#demo1").slippry();
					return false;
				});
			});
		</script>


</html>